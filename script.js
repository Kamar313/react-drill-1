function Counter() {
  return (
    <>
      <Component />
    </>
  );
}

function Component() {
  return (
    <>
      <div className="header">
        <h1 className="heading">All Articles</h1>
        <h3 className="sub-heading">Collection of Best Articles on StartUps</h3>
      </div>
      <DivHolder />
    </>
  );
}

function DivHolder() {
  return (
    <>
      <div className="card-holder">
        <Cards />
        <Cards />
        <Cards />
      </div>
    </>
  );
}

function Cards() {
  return (
    <>
      <div className="cards">
        <div className="inner-div">
          <div className="image-holder">
            <div className="time">
              <p>29</p>
              <p>Mar</p>
            </div>
            <p className="box-image">PHOTOS</p>
          </div>
          <div className="text-holder">
            <h3 className="div-heading">City Lights In New York</h3>
            <h4 className="div-sub-heading">The City That Never Sleep</h4>
            <p className="para">
              Lorem ipsum dolor, sit amet consectetur adipisicing elit. Illo
              deleniti voluptates repellendus temporibus neque voluptas debitis
              quae mollitia. Ex corrupti hic cum!
            </p>
            <p className="timeUpdate">6 Min Ago</p>
            <p className="comments">
              <img src="./image/comments-solid.svg"></img> 6 Comments
            </p>
          </div>
        </div>
      </div>
    </>
  );
}
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<Counter />);
